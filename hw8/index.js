document.addEventListener('DOMContentLoaded', () => {
  function HTML(html) {
    const div = document.createElement('div')
    div.innerHTML = html;
    return div.children[0]
  }
  const app = document.querySelector('#app')

  app.append (HTML(
    `<div>
      <div class="title-wrapper">
        <span class="title"></span>
        <button class="close-btn">&times;</button>
      </div>
      <input type="number" placeholder="Price" />
      <div class="error-wrapper">
        <span class="error">Please enter correct price</span>
      </div>
    </div>`
  ))
  const input = document.querySelector('[placeholder="Price"]')
  const titleBox = document.querySelector('.title-wrapper')
  const title = document.querySelector('.title')
  const btnClose = document.querySelector('.close-btn')
  const error = document.querySelector('.error-wrapper')

  input.addEventListener('focus', (e) => {
    input.style.outlineColor = 'green'
    error.style.display = 'none'
  })
  input.addEventListener('blur', (e) => {
    const inputValue = +e.target.value
    if (inputValue < 0) {
      input.style.borderColor = 'red'
      error.style.display = 'block'
    } else {
      titleBox.style.display = 'flex'
      input.style.borderColor = '#333'
      title.textContent = `Текущая цена: ${inputValue}`
    }
  })

  btnClose.addEventListener('click', () => {
    titleBox.style.display = 'none'
    input.value = ''
  })
})