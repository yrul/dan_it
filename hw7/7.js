
// renderList(document.body, ['test', 1,2,3, 'oleni']);
const array = ["1", "2", "3", "sea", "user", 23];

const renderList = (listOfItems, selector=document.body) => {
  selector.innerHTML= 
  `<ul>
  ${listOfItems.map(item => `<li>${item}`).join('')}
  </ul>`
};

renderList(array, document.body.firstElementChild.nextElementSibling);
// const val = `<h2>HelloWorld, miss it</h2?>`;
// document.body.innerHTML = val;
