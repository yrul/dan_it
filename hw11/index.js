const buttons = document.querySelectorAll('.btn');

const clear = () => buttons.forEach((button) => (button.style.backgroundColor = '#000'));

document.addEventListener('keydown',  (event) => {
    const key = event.key.toLowerCase();
    clear();
    buttons.forEach((button) => {
        if (button.textContent.toLowerCase() === key) {
            button.style.cssText = 'background-color: blue;';
        }
    });
});
