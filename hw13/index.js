const switchTheme = document.querySelector('#myonoffswitch');
const themeStyle = document.querySelector('#stylesheet');

const theme = localStorage.getItem('theme');

if (!theme) {
    localStorage.setItem('theme', 'light');
    switchTheme.checked = false;
} else {
    themeStyle.setAttribute('href', `${theme}.css`);
    theme === 'light'
    ? (switchTheme.checked = false) : (switchTheme.checked = true);
}

switchTheme.addEventListener('change', () => {
    if (switchTheme.checked) {
        themeStyle.setAttribute('href', 'dark.css');
        localStorage.setItem('theme', 'dark');
    } else {
        themeStyle.setAttribute('href', 'light.css');
        localStorage.setItem('theme', 'light');
    }
})