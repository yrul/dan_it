const filterBy = (arr, dataType) => {
  const result = [];
  for (let item=0; item<arr.length; item++) {
    if(typeof arr[item] !== dataType) {
      result.push(arr[item]);
    };
  }
return result;
}

const filterBy1 = (array, dataType1) => array.filter(item => typeof item !== dataType1);

console.log(filterBy(['hello', 'world', 23, '23', null], 'null'));
console.log(filterBy1(['hello', 'world', 23, '23', null], 'string'))