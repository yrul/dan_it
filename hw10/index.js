const inputWrappers = document
    .querySelectorAll('.input-wrapper')
    .forEach((element) => {
        element.addEventListener('click', (event) => {
            const target = event.target;
            if(target.nodeName === 'I') {
                if (target.previousElementSibling.type === 'password') {
                    target.previousElementSibling.setAttribute('type', 'text');
                    target.classList.add('fa-eye-slash');
                }else {
                    target.previousElementSibling.setAttribute('type', 'password');
                    target.classList.remove('fa-eye-slash');
                }
            }
        });
    });

const form = document.querySelector('.password-form');

form.addEventListener('submit', (event) => {
    event.preventDefault();
    const password = form.querySelector('#password').value;
    const confirmPassword = form.querySelector('#password-check').value;

    if (password === confirmPassword) {
        alert('You are welcome')
    } else {
        alert('Нужно ввести одинаковые значения')
    }

    form.reset();
})


