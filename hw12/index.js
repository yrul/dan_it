const images = document.querySelectorAll('.image-to-show');
const stopShow = document.querySelector('.stop-show')
const restartShow = document.querySelector('.restart')

let interval = null;

const timeStep = 3000;

let i=0;

let current = 0;

// images[i].style.display = 'block';
//
// i++;

const showImage = () => {
    interval = setInterval(() => {
        images.forEach((img) => {
            img.style.display = 'none';
        });

        if (i === images.length) {
            i=0;
        }
        images[i].style.display = 'block';
        i++;
    }, timeStep);
};

showImage();

stopShow.addEventListener('click', () => {
    current = i;
    clearInterval(interval);
})

restartShow.addEventListener('click', () => {
    i = current;
    showImage();
});